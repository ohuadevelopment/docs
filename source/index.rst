.. ohua documentation master file, created by
   sphinx-quickstart on Thu Dec  8 16:04:13 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ohua's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   stateful-functions
   importing



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

